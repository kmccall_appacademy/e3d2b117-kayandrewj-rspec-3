class Array
  def sum
    final = self.first || 0
    self.drop(1).each { |a| final = final + a}
    final
  end
end

class Array

  def square!
    self.map! { |x| x = x*x }
  end

  def square
    dup.square!
  end
end

class Array
  def my_uniq
    uniques = []

    self.each do |element|
      next if uniques.include?(element)
      uniques << element
    end

  uniques

  end
end

class Array
  def two_sum
    pairs = []

    self.count.times do |x|
      (x + 1).upto (self.count - 1) do |y|
        pairs << [x, y] if self[x] + self[y] == 0
      end
    end
    pairs
  end
end

class Array
  def median
    return nil if self.empty?
    sorted_arr = self.sort
    indexes = self.length-1
    indexes%2 == 0 ? sorted_arr[indexes/2] : (sorted_arr[indexes/2] + sorted_arr[(indexes/2)+1])/2.0
  end
end

class Array
  def my_transpose
    cols = []
    self.length.times do |i|
      row = []
      sub_matrix = self[i]
      sub_matrix.length.times do |j|
        row << self[j][i]
      end
      cols << row
    end
    cols
  end
end
