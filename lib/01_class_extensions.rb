class String
  def caesar(shift)
    string = self.downcase
    shifted_str = ""
    string.each_byte do |ascii|
      letter_code = ascii - "a".ord
      shifted_char_code = (letter_code + shift) % 26
      shifted_str << ("a".ord + shifted_char_code).chr
    end
    shifted_str
  end
end

class Hash
  def difference(other_hash)
    final_hash = {}
    self.keys.each { |key| final_hash[key] = self[key] unless other_hash.key?(key)}
    other_hash.keys.each { |key| final_hash[key] = other_hash[key] unless self.key?(key)}
    final_hash
  end
end

# After I wrote the stringify solution I looked at the solution
# and noticed an array was used, rather than a hash. Is there
# an advantage to either? This one was easier to reason with
# for me; one less thing to abstract. 

class Fixnum
  def stringify(base)
    new_str = ""
    base_hash = {
      0 => "0",
      1 => "1",
      2 => "2",
      3 => "3",
      4 => "4",
      5 => "5",
      6 => "6",
      7 => "7",
      8 => "8",
      9 => "9",
      10 => "a",
      11 => "b",
      12 => "c",
      13 => "d",
      14 => "e",
      15 => "f"
    }
    left_over = self
    while left_over >= 1
      new_str << base_hash[left_over%base]
      left_over = left_over/base
    end
    new_str.reverse
  end
end

# Bonus: Refactor your `String#caesar` method to work with strings containing
# both upper- and lowercase letters.
